#percentage of X in file data.csv:
echo "($(grep -o 'X' data.csv | tr -d '\n' | wc -c) / $(cut -c1- data.csv | tr -d '\n' | wc -c))*100" | bc -l
